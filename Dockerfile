FROM node:6
EXPOSE 80
ENV NODE_PORT 80
WORKDIR /prerenderer
ADD package.json package.json
RUN npm install
ADD . .
CMD npm start
