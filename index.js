'use strict';

const koa = require('koa');
const url = require('url');
const path = require('path');
const phantomjs = require('phantomjs-prebuilt').path;
const execFile = require('mz/child_process').execFile;
const app = koa();

const allowedDomain = process.env.PAPAYAPODS_DOMAIN || 'papayapods.com';

app.use(function * () {
  const urlToFetch = this.request.path.substring(1);
  const domain = url.parse(urlToFetch).hostname;
  if (domain && domain.length > 1 && domain.indexOf(allowedDomain) > -1) {
    this.body = (yield execFile(phantomjs,
      [path.resolve('./loadpage.phantom.js'), urlToFetch]))[0];
    this.status = 200;
  } else {
    this.status = 401;
  }
});
app.listen(process.env.NODE_PORT || 3000, () => { });
